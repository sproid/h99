import System.Random
import Control.Monad
import Data.List
import Data.Numbers.Primes (primes)

-- 1
-- Last element of list
myLast :: [a] -> a
myLast [] = undefined
myLast [x] = x
myLast (x:xs) = myLast xs

-- 2
-- Last but one element of list
myButLast :: [a] -> a
myButLast = last . init

-- 3
-- Find K'th element in a list
elAt :: [a] -> Int -> a
elAt [] _ = undefined
elAt (x:xs) n
    | n > 1 = elAt xs (n-1)
    | n <= 1 = x

-- 4
-- Find nubmer of elements in list
myLength :: [a] -> Int
myLength [] = 0
myLength (_:xs) = 1 + myLength xs

-- 5
-- Reverse a list
myReverse :: Eq a => [a] -> [a]
myReverse [] = []
myReverse (x:xs) = myReverse xs ++ [x]

-- 6
-- Find out if a list is a palindrome
isPalindrome :: Eq a => [a] -> Bool
isPalindrome [] = True
isPalindrome xs = xs == reverse xs

-- 7
-- Flatten nested list structure
data NestedList a = Elem a | List [NestedList a]
myFlatten :: NestedList a -> [a]
myFlatten (Elem x) = [x]
myFlatten (List (x:xs)) = myFlatten x ++ myFlatten (List xs)
myFlatten (List []) = []

-- 8
-- Eliminate consecutive duplicates in a list
compress :: Eq a => [a] -> [a]
compress (x:ys@(y:_))
    | x == y = compress ys
    | otherwise = x : compress ys
compress ys = ys

-- 9
-- Pack consecutives in lists
pack :: Eq a => [a] -> [[a]]
pack (x:xs) = let (first,last) = span (==x) xs
                in (x:first) : pack last
pack [] = []

-- 10
-- Run length encoding
encode xs = [(length x, head x) | x <- pack xs]

-- 11
-- Modified run length encoding
data ListItem a = Single a | Multiple Int a
    deriving (Show)
encodeModded :: Eq a => [a] -> [ListItem a]
encodeModded xs = [encodeHelper x | x <- encode xs]
    where
        encodeHelper (1, x) = Single x
        encodeHelper (n, x) = Multiple n x

-- 12
-- Decode a run length encoding
decoded :: [ListItem a] -> [a]
decoded xs = decodeHelper xs
    where
        decodeHelper [] = []
        decodeHelper (x:xs) = unpack x ++ decodeHelper xs
            where
                unpack (Single x) = [x]
                unpack (Multiple 0 x) = []
                unpack (Multiple n x) = x : unpack (Multiple (n-1) x)

-- 13
-- Direct run lenght encoding
-- Solved already in 11 by mistake

-- 14
-- Duplicate the elements in a list
dupli :: [a] -> [a]
dupli [] = []
dupli (x:xs) = x : x : dupli xs

-- 15
-- Duplicate the elements in a list
repli :: [a] -> Int -> [a]
repli [] _ = []
repli (x:xs) n = replicate n x ++ repli xs n

-- 16
-- Drop every n'th element of a list
myDrop :: Eq a => [a] -> Int -> [a]
myDrop [] _ = []
myDrop xs n = dropHelper xs n 1
                where
                    dropHelper [] _ _ = []
                    dropHelper (x:xs) n i
                        | mod i n == 0 = dropHelper xs n (i+1)
                        | otherwise = x : dropHelper xs n (i+1)

-- 17
-- Split the list into two parts, first list's lenght is given
mySlice :: [a] -> Int -> ([a], [a])
mySlice [] _ = ([], [])
mySlice xs i = (take i xs, rest i xs)
                where
                    rest _ [] = []
                    rest 0 (x:xs) = x : rest 0 xs
                    rest i (x:xs) = rest (i-1) xs

-- 18
-- Slice between, i'th and k'th
-- between i'th and k'th indices
slicer :: [a] -> Int -> Int -> [a]
slicer xs i k = back (front xs i) (k-i+1)
                where
                    front xs 1 = xs
                    front (x:xs) i = front xs (i-1)
                    back [] _ = []
                    back xs 0 = []
                    back (x:xs) k = x : back xs (k-1)

-- 19
-- Rotate a list N places to the left
rotater :: [a] -> Int -> [a]
rotater xs n = take len . drop (mod n len) . cycle $ xs
                where len = length xs

-- 20
-- Drop elem at k'th index return tuple with deleted element
dropper :: Int -> [a] -> (Maybe a, [a])
dropper _ [] = (Nothing, [])
dropper 1 (x:xs) = (Just x, xs)
dropper n (x:xs) = (fst $ dropper (n-1) xs, x : snd (dropper (n-1) xs))

-- 21
-- Insert elem at given position in list
inserter :: a -> [a] -> Int -> [a]
inserter b [] _ = [b]
inserter b (x:xs) 1 =  b : x : xs
inserter b (x:xs) n =  x : inserter b xs (n-1)

-- 22
-- Range of ints over a given range
range :: Int -> Int -> [Int]
range i k = [i..k]

-- 23
-- Extract a given nr of randomly selected elements form the list
rndSelect :: [a] -> Int -> IO [a]
rndSelect [] _ = return []
rndSelect xs n = do pos <- replicateM n $ getStdRandom $ randomR (0, length xs - 1)
                    return [xs!!p | p <- pos]
-- Well this random generation is more complicated than I thought
-- Interesting how Monad IO works, finally learned a bit about that
-- !! => indexing of lists, remember

-- 24
-- Draw n different random numbers from the set [1..M]
diffSelect :: Int -> Int -> IO [Int]
diffSelect 0 0 = return []
diffSelect n m = do pos <- replicateM n $ getStdRandom $ randomR (0, m-1)
                    return [[1..m]!!i | i <- pos]

-- 25
-- Generate a random permutation of element in a list
rndPermu :: [a] -> IO [a]
rndPermu [] = return []
rndPermu xs = do
                gen <- getStdRandom random
                return $ perms !! mod gen (length perms - 1)
                where
                    perms = permutations xs
-- very inefficient, exponential time
-- more interesting approach https://wiki.haskell.org/99_questions/Solutions/25

-- 26
-- Generate the combinations of K distinct objects chosen from the N elements of a list 
combinations :: Int -> [a] -> [[a]]
combinations 0 _ = [[]]
combinations n xs = [y:ys | y:xs' <- tails xs
                          , ys <- combinations (n-1) xs']
-- gave up, tired

-- 27
-- In how many ways can a group of 9 people work in 3 disjoint subgroups of 2, 3
-- and 4 persons? Write a function that generates all the possibilities and
-- returns them in a list. 

-- I don't understand neither the solution nor the problem, yet
-- TODO this

-- 28
-- Sort a list of lists according to the length of the lists
-- Using mergesort
lfsort :: Ord a => [[a]] -> [[a]]
lfsort [] = []
lfsort [x] = [x]
lfsort as = merge (lfsort $ first as) (lfsort $ second as)
    where
        first xs = take (length xs `div` 2) xs
        second xs = drop (length xs `div` 2) xs
        merge xs [] = xs
        merge [] ys = ys
        merge (x:xs) (y:ys)
            | length x <= length y = x:merge xs (y:ys)
            | otherwise = y:merge (x:xs) ys

-- 29 -- the problems just end? tf?

-- 31
-- Determine whether a given int is a prime
isPrime :: Integer -> Bool
isPrime x = not $ foldr (||) False $ map (\y -> mod x y == 0) [2..x`div`2]

-- 32
-- Determine the GCD of two positive integers
-- https://en.wikipedia.org/wiki/Euclidean_algorithm
myGcd :: Integral a => a -> a -> a
myGcd a b
    | a > b = myGcd (a-b) b
    | a < b = myGcd a (b-a)
    | a == b = a

-- 33
-- Determine if two positive integers are coprime, that is if their GCD is 1
coprime :: Integral a => a -> a -> Bool
coprime a b = myGcd a b == 1

-- 34
-- Calculate Euler's totient function phi(m)
totient :: Int -> Int
totient 1 = 1
totient x = length [y | y <- [1..x-1], coprime x y]

-- 35
--  Determine the prime factors of a given positive integer. Construct a flat
--  list containing the prime factors in ascending order. 
primeFactors :: Integer -> [Integer]
primeFactors 1 = [1]
primeFactors x = undefined











main = undefined
